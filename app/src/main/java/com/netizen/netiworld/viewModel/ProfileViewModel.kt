package com.netizen.netiworld.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.repository.ProfileRepository

class ProfileViewModel(application: Application) : AndroidViewModel(application)  {

    private val profileRepository = ProfileRepository(application)

    val customNetiID = profileRepository.customNetiID
    var fullName = profileRepository.fullName
    var basicMobile = profileRepository.basicMobile
    var basicEmail = profileRepository.basicEmail
    var userStatus = profileRepository.userStatus
    var village = profileRepository.village
    var upozilla = profileRepository.upozilla
    var district = profileRepository.district
    var division = profileRepository.division
    var imagePath = profileRepository.imagePath
    var photoFileContent = profileRepository.photoFileContent
    var gender = profileRepository.gender
    var religion = profileRepository.religion
    var dateOfBirth = profileRepository.dateOfBirth
    var age = profileRepository.age

    fun getProfileInfo() {
        if (fullName.value.isNullOrEmpty()) {
            profileRepository.getUserProfileInfoData()
        }
    }

    fun getProfileImage(imagePath: String) {
        if (photoFileContent.value.isNullOrEmpty()) {
            profileRepository.getProfilePhoto(imagePath)
        }
    }

    class ProfileViewModelFactory(val application: Application): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
                return ProfileViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}