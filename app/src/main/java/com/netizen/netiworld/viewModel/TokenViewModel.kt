package com.netizen.netiworld.viewModel

import android.app.Application
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap.CompressFormat
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Base64
import androidx.lifecycle.*
import com.netizen.netiworld.model.Problem
import com.netizen.netiworld.model.TokenList
import com.netizen.netiworld.model.TokenSubmitPostData
import com.netizen.netiworld.repository.TokenRepository
import com.netizen.netiworld.utils.Loaders
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke
import java.io.File
import java.io.FileOutputStream
import java.lang.reflect.Array


class TokenViewModel(application: Application) : AndroidViewModel(application) {

    private val tokenRepository = TokenRepository(application)

    val solvedAndPendingTokenList = tokenRepository.solvedAndPendingTokenList
    private val problemModuleList = tokenRepository.problemModuleList
    private val problemTypeList = tokenRepository.problemTypeList
    val tokenList = tokenRepository.tokenList
    val token = MutableLiveData<TokenList>()

    val isProblemModuleListFound = tokenRepository.isProblemModuleListFound
    val isProblemTypeListFound = tokenRepository.isProblemTypeListFound
    val isDownloading = tokenRepository.isDownloading
    private val downloadedAttachment = tokenRepository.downloadedAttachment

    val tempProblemModuleList = Transformations.map(problemModuleList) {
        getTempProblemModuleList(it)
    }
    val tempProblemTypeList = Transformations.map(problemTypeList) {
        getTempProblemTypeList(it)
    }
//    val isImageSaved = Transformations.map(downloadedAttachment) {
//        saveImage(it)
//    }

    var attachmentName = MutableLiveData<String>()
    var isProblemModuleEmpty = MutableLiveData<Boolean>()
    var isProblemTypeEmpty = MutableLiveData<Boolean>()
    var isProblemDetailsEmpty = MutableLiveData<Boolean>()
    var isContactNumberEmpty = MutableLiveData<Boolean>()
    var isAttachmentEmpty = MutableLiveData<Boolean>()

    private var parentCategoryId: String? = null
    private var coreCategoryId: String? = null
    private var attachmentBase64: String? = null
    var downloadImageUrl: String? = null

    fun getSolvedAndPendingTokenList() {
        tokenRepository.getSolvedAndPendingTokenList()
    }

    fun getProblemModuleList() {
        if (problemModuleList.value.isNullOrEmpty()) {
            tokenRepository.getProblemModuleList()
        }
    }

    fun getTokenList() {
        if (tokenList.value.isNullOrEmpty()) {
            tokenRepository.getTokenList()
        }
    }

    private fun getTempProblemModuleList(problemModuleList: List<Problem>): ArrayList<String> {
        val tempProblemModuleList = ArrayList<String>()

        problemModuleList.forEach { problemModule ->
            tempProblemModuleList.add(problemModule.getCategoryName().toString())
        }

        return tempProblemModuleList
    }

    private fun getTempProblemTypeList(problemTypeList: List<Problem>): ArrayList<String> {
        val tempProblemTypeList = ArrayList<String>()

        problemTypeList.forEach { problemModule ->
            tempProblemTypeList.add(problemModule.getCategoryName().toString())
        }

        return tempProblemTypeList
    }

    fun getProblemModuleId(problemModuleName: String) {
        problemModuleList.value?.forEach { problemModule ->
            if (problemModule.getCategoryName().equals(problemModuleName, true)) {
                parentCategoryId = problemModule.getCoreCategoryID().toString()
                tokenRepository.getProblemTypeList(parentCategoryId!!)
                return
            }
        }
    }

    fun getProblemTypeId(problemTypeName: String) {
        problemTypeList.value?.forEach { problemType ->
            if (problemType.getCategoryName().equals(problemTypeName, true)) {
                coreCategoryId = problemType.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getFileName(context: Context, uri: Uri) {
        val returnCursor: Cursor = context.contentResolver.query(uri, null, null, null, null)!!
        val nameIndex: Int = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name: String = returnCursor.getString(nameIndex)
        attachmentName.value = name

        returnCursor.close()
    }

    fun getRealPathFromURI(context: Context, contentUri: Uri) {
        val projection = arrayOf(MediaStore.Audio.Media.DATA)
        val cursor: Cursor = context.contentResolver.query(contentUri, projection, null, null, null)!!
        val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
        cursor.moveToFirst()

        val path = cursor.getString(columnIndex)
//        Log.e("Real Path", "$path")
        cursor.close()

        encodeToBase64(File(path))
    }

    private fun encodeToBase64(file: File) {
        val encodedString = Base64.encodeToString(file.readBytes(), Base64.NO_WRAP)
//        Log.e("Encoded String", encodedString)
        attachmentBase64 = encodedString
    }

    fun validationSubmitToken(details: String?, contactNumber: String?) {
        when {
            parentCategoryId.isNullOrEmpty() -> {
                isProblemModuleEmpty.value = true
                Loaders.apiError.value = "Problem module is empty!"
            }
            coreCategoryId.isNullOrEmpty() -> {
                isProblemTypeEmpty.value = true
                Loaders.apiError.value = "Problem type is empty!"
            }
            details.isNullOrEmpty() -> {
                isProblemDetailsEmpty.value = true
                Loaders.apiError.value = "Problem details is empty!"
            }
            contactNumber.isNullOrEmpty() -> {
                isContactNumberEmpty.value = true
                Loaders.apiError.value = "Contact number is empty!"
            }
            attachmentName.value.isNullOrEmpty() -> {
                isAttachmentEmpty.value = true
                Loaders.apiError.value = "Attachment is empty!"
            }
            else -> {
                val tokenTypeInfoDTO = TokenSubmitPostData.TokenTypeInfoDTO()
                tokenTypeInfoDTO.setCoreCategoryID(coreCategoryId!!.toInt())

                val tokenSubmitPostData = TokenSubmitPostData()
                tokenSubmitPostData.setAttachSaveOrEditable(true)
                tokenSubmitPostData.setAttachmentName(attachmentName.value)
                tokenSubmitPostData.setAttachContent(attachmentBase64)
                tokenSubmitPostData.setTokenContact(contactNumber)
                tokenSubmitPostData.setTokenDetails(details)
                tokenSubmitPostData.setTokenSource("Netiworld")
                tokenSubmitPostData.setTokenTypeInfoDTO(tokenTypeInfoDTO)

                tokenRepository.submitToken(tokenSubmitPostData)
            }
        }
    }

    fun downloadImage() {
        tokenRepository.downloadTokenAttachment(downloadImageUrl)
    }

    suspend fun saveImage() {
        Dispatchers.IO {
            val decodedString: ByteArray = Base64.decode(downloadedAttachment.value, Base64.DEFAULT)
            val bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            var splitImageName = listOf<String>()
            if (!downloadImageUrl.isNullOrEmpty()) {
                splitImageName = downloadImageUrl!!.split("/")
            }

            val filename = splitImageName[splitImageName.size - 1]
            val file = Environment.getExternalStorageDirectory().absolutePath + "/Download/"
            val dest = File(file, filename)

            try {
                val out = FileOutputStream(dest)
                bitmap.compress(CompressFormat.PNG, 100, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    class TokenViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(TokenViewModel::class.java)) {
                return TokenViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel Class")
        }
    }
}