package com.netizen.netiworld.viewModel

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.CursorLoader
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Base64
import android.util.Log
import androidx.lifecycle.*
import com.netizen.netiworld.model.*
import com.netizen.netiworld.repository.BankRepository
import com.netizen.netiworld.utils.Loaders
import java.io.ByteArrayOutputStream
import java.io.File


class BankAccountViewModel(application: Application) : AndroidViewModel(application) {

    private val bankRepository = BankRepository(application)

    private var bankList = bankRepository.bankList
    private var districtList = bankRepository.districtList
    private var branchList = bankRepository.branchList
    var userBankAccountList = bankRepository.userBankAccountList
    var tagList = bankRepository.tagList
    private var tagTypeList = bankRepository.tagTypeList

    var tempBankList = Transformations.map(bankList) {
        getTempBankList(it)
    }
    var tempDistrictList = Transformations.map(districtList) {
        getTempDistrictList(it)
    }
    var tempBranchList = Transformations.map(branchList) {
        getTempBranchList(it)
    }
    var tempTagTypeList = Transformations.map(tagTypeList) {
        getTempTagTypeList(it)
    }

    var isBankAccountListFound = bankRepository.isBankAccountListFound
    var isDistrictListFound = bankRepository.isDistrictListFound
    var isBranchListFound = bankRepository.isBranchListFound
    var routingNumber = MutableLiveData<String?>()
    var chequeLeafImageName = MutableLiveData<String?>()
    var otherImageName = MutableLiveData<String?>()
    var isBankEmpty = MutableLiveData<Boolean>()
    var isDistrictEmpty = MutableLiveData<Boolean>()
    var isBranchEmpty = MutableLiveData<Boolean>()
    var isRoutingNumberEmpty = MutableLiveData<Boolean>()
    var isAccountHolderNameEmpty = MutableLiveData<Boolean>()
    var isAccountNumberEmpty = MutableLiveData<Boolean>()
    var isAccountDetailsEmpty = MutableLiveData<Boolean>()
    var isChequeLeafImageEmpty = MutableLiveData<Boolean>()
    var isOtherImageEmpty = MutableLiveData<Boolean>()

    private var bankId: String? = null
    private var districtId: String? = null
    private var branchId: String? = null
    var userBankAccountId: String? = null
    var netiId: String? = null
    private var categoryDefaultCode: String? = null
    private var chequeLeafImage: String? = null
    private var otherImage: String? = null
    var isChequeLeafImage: Boolean? = null

    fun getBankAccountList() {
        if (bankList.value.isNullOrEmpty()) {
            bankRepository.getBankList()
        }
    }

    fun getDistrictList() {
        if (districtList.value.isNullOrEmpty()) {
            bankRepository.getDistrictList()
        }
    }

    private fun getBranchList(bankId: String?, districtId: String?) {
        when {
            bankId.isNullOrEmpty() -> {
                isBankEmpty.value = true
                Loaders.apiError.value = "Please select a bank account."
            }
            districtId.isNullOrEmpty() -> {
                isDistrictEmpty.value = true
                Loaders.apiError.value = "Please select a district."
            }
            else -> {
                bankRepository.getBranchList(bankId, districtId)
            }
         }
    }

    fun getBankList() {
        bankRepository.getUserBankList()
    }

    fun getTagList() {
        if (tagList.value.isNullOrEmpty()) {
            bankRepository.getTagList()
        }
    }

    fun getTagTypeList() {
        if (tagTypeList.value.isNullOrEmpty()) {
            bankRepository.getTagTypeList()
        }
    }

    private fun getTempBankList(bankList: List<BankAccountInfo>): ArrayList<String> {
        val tempBankList = ArrayList<String>()

        bankList.forEach { bank ->
            tempBankList.add(bank.getCategoryName().toString())
        }

        return tempBankList
    }

    private fun getTempDistrictList(districtList: List<BankAccountInfo>): ArrayList<String> {
        val tempDistrictList = ArrayList<String>()

        districtList.forEach { district ->
            tempDistrictList.add(district.getCategoryName().toString())
        }

        return tempDistrictList
    }

    private fun getTempBranchList(branchList: List<BankBranchInfo>): ArrayList<String> {
        val tempBranchList = ArrayList<String>()

        branchList.forEach { branchInfo ->
            tempBranchList.add(branchInfo.getBranchName().toString())
        }

        return tempBranchList
    }

    private fun getTempTagTypeList(tagTypeList: List<TagType>): ArrayList<String> {
        val tempTagTypeList = ArrayList<String>()
        tempTagTypeList.add("Select Type")

        tagTypeList.forEach { tagType ->
            tempTagTypeList.add(tagType.getCategoryName().toString())
        }

        return tempTagTypeList
    }

    fun getBankId(bankName: String) {
        bankList.value?.forEach { bank ->
            if (bank.getCategoryName().equals(bankName, true)) {
                bankId = bank.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getDistrictID(districtName: String) {
        districtList.value?.forEach { district ->
            if (district.getCategoryName().equals(districtName, true)) {
                districtId = district.getCoreCategoryID().toString()
                getBranchList(bankId, districtId)
                return
            }
        }
    }

    fun getBranchId(branchName: String?) {
        branchList.value?.forEach { branch ->
            if (branch.getBranchName().equals(branchName, true)) {
                branchId = branch.getBranchID().toString()
                routingNumber.value = branch.getRoutingNumber().toString()
                return
            }
        }
    }

    fun getCategoryDefaultCode(tagTypeName: String) {
        tagTypeList.value?.forEach { tagType ->
            if (tagType.getCategoryName().equals(tagTypeName, true)) {
                categoryDefaultCode = tagType.getCategoryDefaultCode()
            }
        }
    }

    fun tagBankAccount() {
        when {
            userBankAccountId.isNullOrEmpty() -> {
                Loaders.apiError.value = "Couldn't found user bank account ID!"
            }
            netiId.isNullOrEmpty() -> {
                Loaders.apiError.value = "Couldn't found neti ID!"
            }
            categoryDefaultCode.isNullOrEmpty() -> {
                Loaders.apiError.value = "Couldn't found category default code!"
            }
            else -> {
                val userBasicInfoDTO = TagPostData.UserBasicInfoDTO()
                userBasicInfoDTO.setNetiID(netiId!!.toInt())

                val userBankAccountInfoDTO = TagPostData.UserBankAccountTaggingDTO.UserBankAccountInfoDTO()
                userBankAccountInfoDTO.setUserBankAccId(userBankAccountId!!.toInt())

                val userBankAccountTaggingDTO = TagPostData.UserBankAccountTaggingDTO()
                userBankAccountTaggingDTO.setUserBankAccountInfoDTO(userBankAccountInfoDTO)

                val tagPostData = TagPostData()
                tagPostData.setTaggingTypeDefaultCode(categoryDefaultCode)
                tagPostData.setUserBasicInfoDTO(userBasicInfoDTO)
                tagPostData.setUserBankAccountTaggingDTO(userBankAccountTaggingDTO)

                bankRepository.tagBankAccount(tagPostData)
            }
        }
    }

    private fun bitMapToString(bitmap: Bitmap): String {
        val byteArray = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArray)
        val b = byteArray.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    fun getFileName(context: Context, uri: Uri) {
        val returnCursor: Cursor = context.contentResolver.query(uri, null, null, null, null)!!
        val nameIndex: Int = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name: String = returnCursor.getString(nameIndex)

        if (isChequeLeafImage!!) {
            chequeLeafImageName.value = name
        } else {
            otherImageName.value = name
        }

        returnCursor.close()
    }

    fun getRealPathFromURI(context: Context, contentUri: Uri) {
        val projection = arrayOf(MediaStore.Audio.Media.DATA)
        val cursor: Cursor = context.contentResolver.query(contentUri, projection, null, null, null)!!
        val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
        cursor.moveToFirst()

        val path = cursor.getString(columnIndex)
//        Log.e("Real Path", "$path")
        cursor.close()

        encodeToBase64(File(path))
    }

    private fun encodeToBase64(file: File) {
        val encodedString = Base64.encodeToString(file.readBytes(), Base64.NO_WRAP)
//        Log.e("Encoded String", encodedString)

        if (isChequeLeafImage!!) {
            chequeLeafImage = encodedString
        } else {
            otherImage = encodedString
        }
    }

    @SuppressLint("NewApi")
    fun getRealPathFromURI_API19(
        context: Context,
        uri: Uri?
    ) {
        var filePath = ""
        val wholeID = DocumentsContract.getDocumentId(uri)

        // Split at colon, use second item in the array
        val id = wholeID.split(":").toTypedArray()[1]
        val column =
            arrayOf(MediaStore.Images.Media.DATA)

        // where id is equal to
        val sel = MediaStore.Images.Media._ID + "=?"
        val cursor = context.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            column, sel, arrayOf(id), null
        )
        val columnIndex = cursor!!.getColumnIndex(column[0])
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex)
        }
        cursor.close()

        Log.e("Real Path", "$filePath")
    }


    @SuppressLint("NewApi")
    fun getRealPathFromURI_API11to18(
        context: Context?,
        contentUri: Uri?
    ) {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        var result: String? = null
        val cursorLoader = CursorLoader(
            context,
            contentUri, proj, null, null, null
        )
        val cursor: Cursor = cursorLoader.loadInBackground()
        if (cursor != null) {
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            result = cursor.getString(column_index)
        }

//        return result
    }

    fun getRealPathFromURI_BelowAPI11(
        context: Context,
        contentUri: Uri?
    ) {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor =
            context.contentResolver.query(contentUri!!, proj, null, null, null)
        val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()

//        return cursor.getString(column_index)
    }

    fun checkValidation(
        accountHolderName: String?,
        accountNumber: String?,
        accountDetails: String?) {
        when {
            bankId.isNullOrEmpty() -> {
                isBankEmpty.value = true
            }
            districtId.isNullOrEmpty() -> {
                isDistrictEmpty.value = true
            }
            branchId.isNullOrEmpty() -> {
                isBranchEmpty.value = true
            }
            routingNumber.value.isNullOrEmpty() -> {
                isRoutingNumberEmpty.value = true
            }
            accountHolderName.isNullOrEmpty() -> {
                isAccountHolderNameEmpty.value = true
            }
            accountNumber.isNullOrEmpty() -> {
                isAccountNumberEmpty.value = true
            }
            accountDetails.isNullOrEmpty() -> {
                isAccountDetailsEmpty.value = true
            }
            chequeLeafImage.isNullOrEmpty() -> {
                isChequeLeafImageEmpty.value = true
            }
//            otherImage.isNullOrEmpty() -> {
//                isOtherImageEmpty.value = true
//            }
            else -> {
                val coreCategoryInfoDTO = BankAccountPostData.CoreCategoryInfoDTO()
                coreCategoryInfoDTO.setCoreCategoryId(bankId!!.toInt())

                val coreBankBranchInfoDTO =  BankAccountPostData.CoreBankBranchInfoDTO()
                coreBankBranchInfoDTO.setBranchId(branchId!!.toInt())

                val bankAccountPostData = BankAccountPostData()
                bankAccountPostData.setCoreCategoryInfoDTO(coreCategoryInfoDTO)
                bankAccountPostData.setCoreBankBranchInfoDTO(coreBankBranchInfoDTO)
                bankAccountPostData.setBankAccNumber(accountNumber)
                bankAccountPostData.setBankAccHolderName(accountHolderName)
                bankAccountPostData.setBankNote(accountDetails)
                bankAccountPostData.setChequeSlipEditable(true)
                bankAccountPostData.setChequeSlipName(chequeLeafImageName.value)
                bankAccountPostData.setChequeSlipContent(chequeLeafImage)
                bankAccountPostData.setOthersAttachmentEditable(true)
                bankAccountPostData.setOthersAttachmentName(otherImageName.value)
                bankAccountPostData.setOthersAttachmentContent(otherImage)

                bankRepository.submitBankInfo(bankAccountPostData)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    class BankAccountViewModelFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(BankAccountViewModel::class.java)) {
                return BankAccountViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel Class")
        }
    }
}