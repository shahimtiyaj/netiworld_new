package com.netizen.netiworld.view.fragment.myPoint

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentMyPointBinding
import com.netizen.netiworld.utils.OnSwipeTouchListener
import com.netizen.netiworld.viewModel.HomeViewModel
import com.netizen.netiworld.viewModel.ProfileViewModel


/**
 * A simple [Fragment] subclass.
 */
class MyPointFragment : DialogFragment() {

    private lateinit var binding: FragmentMyPointBinding
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var profileViewModel: ProfileViewModel

    private var isBalanceOpened: Boolean = false
    private var isPurchaseOpened: Boolean = false
    private var isBalanceReportOpened: Boolean = false
    private var isPurchaseReportOpened: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_point, container, false)

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)

        homeViewModel = ViewModelProvider(activity!!,
            HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application))
            .get(HomeViewModel::class.java)

        profileViewModel = ViewModelProvider(activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application))
            .get(ProfileViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog?.window?.attributes?.windowAnimations = R.style.HorizontalDialogAnimation
    }

    private fun initViews() {
        binding.layoutMainView.setOnTouchListener(object : OnSwipeTouchListener(context!!) {
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                dismiss()
            }
        })

        binding.scrollMainView.setOnTouchListener(object : OnSwipeTouchListener(context!!) {
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                dismiss()
            }
        })

        binding.imageViewBack.setOnClickListener {
            dismiss()
        }

        binding.textViewBalance.setOnClickListener {
            if (!isBalanceOpened) {
                showBalancePoint()
            } else {
                hideBalancePoint()
            }

            hidePurchasePoint()
            hideBalanceReportPoint()
            hidePurchaseReportPoint()
        }

        binding.textViewPurchase.setOnClickListener {
            if (!isPurchaseOpened) {
                showPurchasePoint()
            } else {
                hidePurchasePoint()
            }

            hideBalancePoint()
            hideBalanceReportPoint()
            hidePurchaseReportPoint()
        }

        binding.textViewBalanceReport.setOnClickListener {
            if (!isBalanceReportOpened) {
                showBalanceReportPoint()
            } else {
                hideBalanceReportPoint()
            }

            hidePurchaseReportPoint()
            hideBalancePoint()
            hidePurchasePoint()
        }

        binding.textViewPurchaseReport.setOnClickListener {
            if (!isPurchaseReportOpened) {
                showPurchaseReportPoint()
            } else {
                hidePurchaseReportPoint()
            }

            hideBalanceReportPoint()
            hideBalancePoint()
            hidePurchasePoint()
        }

        binding.textViewWallet.setOnClickListener {
            dismiss()
            homeViewModel.isWalletClicked.value = true
        }

        binding.textViewMessage.setOnClickListener {
            dismiss()
            homeViewModel.isMessageClicked.value = true
            findNavController().navigate(R.id.action_homeFragment_to_fragmentMessageRecharge)
        }

        binding.textViewOfferProduct.setOnClickListener {
            dismiss()
            homeViewModel.isOfferProductClicked.value = true
        }

        binding.textViewGeneralProduct.setOnClickListener {
            dismiss()
            homeViewModel.isGeneralProductClicked.value = true
        }

        binding.textViewBank.setOnClickListener {
            dismiss()
            homeViewModel.isBankClicked.value = true
        }

        binding.textViewMessageReport.setOnClickListener {
            dismiss()
            homeViewModel.isMessageLogClicked.value = true
        }

        binding.textViewStatementReport.setOnClickListener {
            dismiss()
            homeViewModel.isStatementReportClicked.value = true
        }

        binding.textViewRevenueReport.setOnClickListener {
            dismiss()
            homeViewModel.isRevenueReportClicked.value = true
        }

        binding.textViewGeneralProductReport.setOnClickListener {
            dismiss()
            homeViewModel.isGeneralProductReportClicked.value = true
        }

        binding.textViewCodeLogReport.setOnClickListener {
            dismiss()
            homeViewModel.isPurchaseCodeReportClicked.value = true
        }

        binding.textViewOfferProductReport.setOnClickListener {
            dismiss()
            homeViewModel.isOfferProductReportClicked.value = true
        }

        binding.textViewWalletReport.setOnClickListener {
            dismiss()
            homeViewModel.isWalletLogReportClicked.value = true
        }
    }

    private fun initObservers() {
        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer {
//            binding.textViewUserName.text = it.getBasicInfo()?.getBasicEmail()
            binding.textViewNetiId.text = "Neti ID: " + it.getBasicInfo()?.getCustomNetiID().toString()
        })

        profileViewModel.fullName.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.textViewUserName.text = it
            }
        })

        profileViewModel.photoFileContent.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                val byteArray = Base64.decode(it, Base64.DEFAULT)
                binding.imageViewUser.setImageBitmap(convertToBitmap(byteArray))
            }
        })
    }

    private fun showBalancePoint() {
        isBalanceOpened = true

//        binding.textViewBalance.setBackgroundDrawable(resources.getDrawable(R.drawable.menu_hover_background))
//        binding.textViewBalance.background = ContextCompat.getDrawable(context!!, R.drawable.menu_hover_background)
        binding.textViewBalance.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewBalance.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewWallet.visibility = View.VISIBLE
        binding.textViewMessage.visibility = View.VISIBLE
    }

    private fun hideBalancePoint() {
        isBalanceOpened = false

        binding.textViewBalance.setBackgroundResource(0)
        binding.textViewBalance.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewWallet.visibility = View.GONE
        binding.textViewMessage.visibility = View.GONE
    }

    private fun showPurchasePoint() {
        isPurchaseOpened = true

        binding.textViewPurchase.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewPurchase.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewGeneralProduct.visibility = View.VISIBLE
        binding.textViewOfferProduct.visibility = View.VISIBLE
    }

    private fun hidePurchasePoint() {
        isPurchaseOpened = false

        binding.textViewPurchase.setBackgroundResource(0)
        binding.textViewPurchase.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewGeneralProduct.visibility = View.GONE
        binding.textViewOfferProduct.visibility = View.GONE
    }

    private fun showBalanceReportPoint() {
        isBalanceReportOpened = true

        binding.textViewBalanceReport.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewBalanceReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewWalletReport.visibility = View.VISIBLE
        binding.textViewMessageReport.visibility = View.VISIBLE
        binding.textViewStatementReport.visibility = View.VISIBLE
        binding.textViewRevenueReport.visibility = View.VISIBLE
    }

    private fun hideBalanceReportPoint() {
        isBalanceReportOpened = false

        binding.textViewBalanceReport.setBackgroundResource(0)
        binding.textViewBalanceReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewWalletReport.visibility = View.GONE
        binding.textViewMessageReport.visibility = View.GONE
        binding.textViewStatementReport.visibility = View.GONE
        binding.textViewRevenueReport.visibility = View.GONE
    }

    private fun showPurchaseReportPoint() {
        isPurchaseReportOpened = true

        binding.textViewPurchaseReport.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewPurchaseReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewGeneralProductReport.visibility = View.VISIBLE
        binding.textViewOfferProductReport.visibility = View.VISIBLE
        binding.textViewCodeLogReport.visibility = View.VISIBLE
    }

    private fun hidePurchaseReportPoint() {
        isPurchaseReportOpened = false

        binding.textViewPurchaseReport.setBackgroundResource(0)
        binding.textViewPurchaseReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewGeneralProductReport.visibility = View.GONE
        binding.textViewOfferProductReport.visibility = View.GONE
        binding.textViewCodeLogReport.visibility = View.GONE
    }

    private fun convertToBitmap(bytes: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
    }
}
