package com.netizen.netiworld.view.fragment.myPoint.bankAccount

import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager

import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.BankListAdapter
import com.netizen.netiworld.databinding.FragmentBankListBinding
import com.netizen.netiworld.model.BankList
import com.netizen.netiworld.model.TagGetData
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.BankAccountViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class BankListFragment : Fragment(), BankListAdapter.OnTagClickListener {

    private lateinit var binding: FragmentBankListBinding
    private lateinit var bankAccountViewModel: BankAccountViewModel

    private var bankList: ArrayList<BankList>? = null
    private var tagList: ArrayList<TagGetData>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_bank_list,
            container,
            false
        )

        initViews()
        initObservers()
        bankAccountViewModel.getBankList()
        bankAccountViewModel.getTagList()
        bankAccountViewModel.getTagTypeList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bankAccountViewModel = ViewModelProvider(
            activity!!,
            BankAccountViewModel.BankAccountViewModelFactory(context!!.applicationContext as Application)
        ).get(BankAccountViewModel::class.java)
    }

    override fun onTagClick(userBankAccountId: String, netiId: String) {
        bankAccountViewModel.userBankAccountId = userBankAccountId
        bankAccountViewModel.netiId = netiId
        TagDialogFragment().show(activity!!.supportFragmentManager, null)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Bank List")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                bankAccountViewModel.getBankList()
            }
        })

        bankAccountViewModel.userBankAccountList.observe(viewLifecycleOwner, Observer {
            bankList = it as ArrayList<BankList>
            binding.textViewFound.text = "Found: ${it.size.toString()}"
            setAdapter()
        })

        bankAccountViewModel.tagList.observe(viewLifecycleOwner, Observer {
            tagList = it as ArrayList<TagGetData>?
            setAdapter()
        })
    }

    private fun setAdapter() {
        if (!bankList.isNullOrEmpty() && !tagList.isNullOrEmpty()) {
            binding.recyclerViewBankList.layoutManager = LinearLayoutManager(context!!)
            binding.recyclerViewBankList.setHasFixedSize(true)
            binding.recyclerViewBankList.adapter = BankListAdapter(context!!, bankList!!, tagList!!, this)
        }
    }
}
