package com.netizen.netiworld.view.fragment.myPoint.balance.wallet

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.DepositReportAdapter
import com.netizen.netiworld.databinding.FragmentWithdrawReportBinding
import com.netizen.netiworld.model.BalanceDepositReportGetData
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.WalletViewModel

/**
 * A simple [Fragment] subclass.
 */
class WithdrawReportFragment : Fragment() {

    private lateinit var binding: FragmentWithdrawReportBinding
    private lateinit var walletViewModel: WalletViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_withdraw_report,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walletViewModel = ViewModelProvider(
            activity!!,
            WalletViewModel.WalletViewModelFactory(context!!.applicationContext as Application)
        ).get(WalletViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Balance Withdraw Report")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        walletViewModel.balanceDepositReportDataList.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                setAdapter(it)
            }
        })
    }

    private fun setAdapter(balanceDepositReportList: List<BalanceDepositReportGetData?>?) {
        binding.recyclerViewRevenueReport.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewRevenueReport.setHasFixedSize(true)
        binding.recyclerViewRevenueReport.adapter = DepositReportAdapter(context!!, balanceDepositReportList)
    }
}
