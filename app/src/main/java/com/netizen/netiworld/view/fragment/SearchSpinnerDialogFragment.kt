package com.netizen.netiworld.view.fragment

import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.SearchSpinnerAdapter
import com.netizen.netiworld.databinding.FragmentSearchSpinnerBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.viewModel.*

/**
 * A simple [Fragment] subclass.
 */
class SearchSpinnerDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentSearchSpinnerBinding

    private var walletViewModel: WalletViewModel? = null
    private var messageViewModel: MessageViewModel? = null
    private var purchaseViewModel: PurchaseViewModel? = null
    private var bankAccountViewModel: BankAccountViewModel? = null
    private var userViewModel: UserViewModel? = null
    private var tokenViewModel: TokenViewModel? = null

    private var spinnerDataList: ArrayList<String>? = null

    private var dataType: String? = null
    private var spinnerType: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_spinner, container, false)

        binding.textViewSearch.text = "Search " + arguments!!.getString("title")

        initViewModels()
        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataType = arguments!!.getString("dataType")?.toLowerCase()
        spinnerType = arguments!!.getInt("spinnerType")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val params = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params
    }

    private fun initViewModels() {
        when (dataType) {
            "wallet" -> {
                walletViewModel = ViewModelProvider(activity!!,
                    WalletViewModel.WalletViewModelFactory(context!!.applicationContext as Application))
                    .get(WalletViewModel::class.java)
            }
            "message" -> {
                messageViewModel = ViewModelProvider(activity!!,
                    MessageViewModel.MessageBalanceViewModelFactory(context!!.applicationContext as Application))
                    .get(MessageViewModel::class.java)
            }
            "purchase" -> {
                purchaseViewModel = ViewModelProvider(activity!!,
                    PurchaseViewModel.PurchaseViewModelFactory(context!!.applicationContext as Application))
                    .get(PurchaseViewModel::class.java)
            }
            "bank" -> {
                bankAccountViewModel = ViewModelProvider(activity!!,
                    BankAccountViewModel.BankAccountViewModelFactory(context!!.applicationContext as Application))
                    .get(BankAccountViewModel::class.java)
            }
            "user" -> {
                userViewModel =
                    ViewModelProvider(activity!!, UserViewModel.SignInViewModelFactory(context!!.applicationContext as Application)).get(
                        UserViewModel::class.java
                    )
            }
            "token" -> {
                tokenViewModel = ViewModelProvider(
                    activity!!,
                    TokenViewModel.TokenViewModelFactory(context!!.applicationContext as Application)
                ).get(TokenViewModel::class.java)
            }
        }
    }

    private fun initViews() {
        binding.textViewClose.setOnClickListener { dismiss() }
        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempSpinnerDataList = ArrayList<String>()
                spinnerDataList?.forEach { spinnerData ->
                    if (spinnerData.toLowerCase().contains(s.toString(), true)) {
                        tempSpinnerDataList.add(spinnerData)
                    }

                    setSpinnerDataList(tempSpinnerDataList)
                }
            }
        })
    }

    private fun initObservers() {
        walletViewModel?.tempAccountNumberList?.observe(viewLifecycleOwner, Observer {
            spinnerDataList = it as ArrayList<String>
            setSpinnerDataList(it)
        })

        messageViewModel?.tempPurchasePointList?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty() && spinnerType == 1) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })

        messageViewModel?.tempMessageTypeList1?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty() && spinnerType == 2) {
                spinnerDataList = it as ArrayList<String>
                setSpinnerDataList(it)
            }
        })

        purchaseViewModel?.tempProductList?.observe(viewLifecycleOwner, Observer {
            spinnerDataList = it
            setSpinnerDataList(it)
        })

        bankAccountViewModel?.tempBankList?.observe(viewLifecycleOwner, Observer {
            if (spinnerType == 1) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })

        bankAccountViewModel?.tempDistrictList?.observe(viewLifecycleOwner, Observer {
            if (spinnerType == 2) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })

        bankAccountViewModel?.tempBranchList?.observe(viewLifecycleOwner, Observer {
            if (spinnerType == 3) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })

        //Sign Up Page
        userViewModel?.tempDistrictList?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty() && spinnerType == 1) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })

        userViewModel?.tempAreaList?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty() && spinnerType == 2) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })

        tokenViewModel?.tempProblemModuleList?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty() && spinnerType == 1) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })

        tokenViewModel?.tempProblemTypeList?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty() && spinnerType == 2) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })

        Loaders.searchValue1?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Loaders.searchValue1?.postValue(null)
                dismiss()
            }
        })

        Loaders.searchValue2?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Loaders.searchValue2?.postValue(null)
                dismiss()
            }
        })

        Loaders.searchValue3?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Loaders.searchValue3?.postValue(null)
                dismiss()
            }
        })
    }

    private fun setSpinnerDataList(tempList: List<String>) {
        binding.recyclerViewSearchSpinnerData.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewSearchSpinnerData.setHasFixedSize(true)
        binding.recyclerViewSearchSpinnerData.adapter = SearchSpinnerAdapter(context!!, tempList, spinnerType)
    }
}
