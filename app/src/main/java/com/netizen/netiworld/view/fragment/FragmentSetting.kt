package com.netizen.netiworld.view.fragment

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentSettingBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.utils.UserSession
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty

class FragmentSetting : Fragment() {

    private lateinit var binding: FragmentSettingBinding

    private var profileViewModel: ProfileViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            this,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false)

        binding.lifecycleOwner = this

        initViews()

        initObservables()

        binding.lifecycleOwner = this

        profileViewModel?.getProfileInfo()

        return binding.root
    }

    private fun initViews() {
        binding.profile.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentSetting_to_fragmentProfile)
        }

        binding.token.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentSetting_to_submitTokenFragment)
        }

        binding.logout.setOnClickListener {
          //  val userSession = UserSession(context)
          //  userSession.removeUserLogInSession()
            findNavController().navigate(R.id.signInFragment)
        }

        binding.changePassword.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentSetting_to_fragmentChangePassward)
        }

        binding.backBtn.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun initObservables() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError?.value = null
            }
        })

        profileViewModel?.fullName?.observe(viewLifecycleOwner, Observer { fullName ->
            if (!fullName.isNullOrEmpty()) {
                binding.txtProfileName.text = fullName
            }
        })

        profileViewModel?.customNetiID?.observe(viewLifecycleOwner, Observer { customNetiId ->
            if (!customNetiId.isNullOrEmpty()) {
                binding.txtNetiId.text = "Neti ID : " + customNetiId
            }
        })

        profileViewModel?.imagePath?.observe(viewLifecycleOwner, Observer { imagepath ->
            if (!imagepath.isNullOrEmpty()) {
                profileViewModel?.getProfileImage(imagepath)
            }
        })

        profileViewModel?.photoFileContent?.observe(
            viewLifecycleOwner,
            Observer { photoFileContent ->
                if (!photoFileContent.isNullOrEmpty()) {
                    val theByteArray: ByteArray? = Base64.decode(photoFileContent, Base64.DEFAULT)
                    binding.profilePicId.setImageBitmap(theByteArray?.let {
                        convertToBitmap(it)
                    })
                }
            })
    }

    private fun convertToBitmap(b: ByteArray): Bitmap {
        Log.d("ArraySize", b.size.toString())
        return BitmapFactory.decodeByteArray(b, 0, b.size)
    }
}
