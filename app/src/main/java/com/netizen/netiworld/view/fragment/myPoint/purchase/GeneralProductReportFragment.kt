package com.netizen.netiworld.view.fragment.myPoint.purchase

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.GeneralProductReportAdapter
import com.netizen.netiworld.databinding.FragmentGeneralProductReportBinding
import com.netizen.netiworld.model.GeneralProductGetData
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.PurchaseViewModel

/**
 * A simple [Fragment] subclass.
 */
class GeneralProductReportFragment : Fragment() {

    private lateinit var binding: FragmentGeneralProductReportBinding
    private lateinit var purchaseViewModel: PurchaseViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_general_product_report,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        purchaseViewModel = ViewModelProvider(
            activity!!,
            PurchaseViewModel.PurchaseViewModelFactory(context!!.applicationContext as Application)
        ).get(PurchaseViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "General Product Report")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        purchaseViewModel.generalProductReportList.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                setAdapter(it)
            }
        })
    }

    private fun setAdapter(generalProductReportList: List<GeneralProductGetData?>?) {
        binding.recyclerViewRevenueReport.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewRevenueReport.setHasFixedSize(true)
        binding.recyclerViewRevenueReport.adapter = GeneralProductReportAdapter(context!!, generalProductReportList)
    }
}
