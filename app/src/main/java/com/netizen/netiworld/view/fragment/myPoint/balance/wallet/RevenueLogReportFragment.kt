package com.netizen.netiworld.view.fragment.myPoint.balance.wallet

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.RevenueLogReportAdapter
import com.netizen.netiworld.databinding.FragmentRevenueLogReportBinding
import com.netizen.netiworld.model.RevenueLogGetData
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.MessageViewModel

/**
 * A simple [Fragment] subclass.
 */
class RevenueLogReportFragment : Fragment() {

    private lateinit var binding: FragmentRevenueLogReportBinding
    private lateinit var messageViewModel: MessageViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_revenue_log_report, container, false)

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        messageViewModel = ViewModelProvider(
            activity!!,
            MessageViewModel.MessageBalanceViewModelFactory(context!!.applicationContext as Application)
        ).get(MessageViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Revenue Log Report")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        messageViewModel.revenueLogList.observe(viewLifecycleOwner, Observer { revenueLogList ->
            if (!revenueLogList.isNullOrEmpty()) {
                setAdapter(revenueLogList)
            }
        })
    }

    private fun setAdapter(revenueLogList: List<RevenueLogGetData?>?) {
        binding.recyclerViewRevenueReport.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewRevenueReport.setHasFixedSize(true)
        binding.recyclerViewRevenueReport.adapter = RevenueLogReportAdapter(context!!, revenueLogList)
    }
}
