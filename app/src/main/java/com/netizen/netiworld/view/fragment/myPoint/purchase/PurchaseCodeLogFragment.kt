package com.netizen.netiworld.view.fragment.myPoint.purchase

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.PurchaseCodeLogPagerAdapter
import com.netizen.netiworld.databinding.FragmentPurchaseCodeLogBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import es.dmoral.toasty.Toasty

class PurchaseCodeLogFragment : Fragment() {

    private lateinit var binding: FragmentPurchaseCodeLogBinding
    private lateinit var adapter: PurchaseCodeLogPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_purchase_code_log,
            container,
            false
        )

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        binding.viewPager.adapter = PurchaseCodeLogPagerAdapter(childFragmentManager)
        binding.viewPager.offscreenPageLimit = 2
        binding.tabLayout.post { binding.tabLayout.setupWithViewPager(binding.viewPager) }

        initViews()
        initObservers()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Purchase Code Log")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })
    }
}
