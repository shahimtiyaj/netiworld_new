package com.netizen.netiworld.view.fragment.user

import android.app.Application
import android.os.Bundle
import android.text.TextUtils
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentSignInBinding
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.utils.UserSession
import com.netizen.netiworld.view.activity.MainActivity
import com.netizen.netiworld.viewModel.UserViewModel
import es.dmoral.toasty.Toasty

class FragmentSignIn : Fragment() {

    private lateinit var binding: FragmentSignInBinding
    private var userViewModel: UserViewModel? = null

    private var isPasswordVisible = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = ViewModelProvider(
            this,
            UserViewModel.SignInViewModelFactory(context!!.applicationContext as Application)
        ).get(UserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)

        MainActivity.hideBottomNavigation()
        initViews()
        initObservables()
        binding.lifecycleOwner = this

        return binding.root
    }

    private fun initViews() {
        binding.imageViewPasswordToggle.setOnClickListener {
            if (isPasswordVisible) {
                isPasswordVisible = false
                binding.inputUserPassword.transformationMethod = PasswordTransformationMethod()
                binding.imageViewPasswordToggle.setImageResource(R.drawable.ic_visibility_off_white_24dp)
            } else {
                isPasswordVisible = true
                binding.inputUserPassword.transformationMethod = null
                binding.imageViewPasswordToggle.setImageResource(R.drawable.ic_visibility_white_24dp)
            }
        }

        binding.signupHere.setOnClickListener {
            findNavController().navigate(R.id.action_signInFragment_to_fragmentSignUpOne)
        }

        binding.forgotPassword.setOnClickListener {
            findNavController().navigate(R.id.action_signInFragment_to_fragmentForgotPasswordOne)
        }

        binding.rememberCheck.setOnClickListener {
            if (binding.rememberCheck.isChecked){
                val userSession = UserSession(context)
               userSession.createUserLogInSession()
               userSession.setUserCredentials(binding.inputUserName.text.toString())
            }
        }
    }

    private fun initObservables() {
        binding.signin.setOnClickListener {
            userViewModel?.onLogInClick(
                binding.inputUserName.text.toString(),
                binding.inputUserPassword.text.toString()
            )
        }

        userViewModel?.isUserNameEmpty?.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.inputUserName.error=resources.getString(R.string.user_name_error)
                binding.inputUserName.requestFocus()
            }
        })

        userViewModel?.isPasswordEmpty?.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.inputUserPassword.error = resources.getString(R.string.password_error)
                binding.inputUserPassword.requestFocus()
            }
        })

        userViewModel?.accessToken?.observe(viewLifecycleOwner, Observer { token ->
            if (token != null) {
                AppPreferences(context).setToken(token)
            }
        })

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        userViewModel?.isLoggedIn?.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                val userSession = UserSession(context)
                userSession.createUserLogInSession()
                userSession.setUserCredentials(binding.inputUserName.text.toString())
                findNavController().navigate(R.id.action_signInFragment_to_homeFragment)
                userViewModel?.isLoggedIn?.value = false
            }
        })

        Loaders.success?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
            }
        })

        Loaders.error?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {

                LoginFailedDialogFragment()
                    .show(activity!!.supportFragmentManager, null)

                Loaders.error?.value = null
            }
        })

        val userName = UserSession(context).getUserName()
        if (!TextUtils.isEmpty(userName)) {
            binding.inputUserName.setText(userName)
        }

        val userPass = UserSession(context).getUserPass()
        if (!TextUtils.isEmpty(userPass)) {
            binding.inputUserPassword.setText(userPass)
        }
    }
}
