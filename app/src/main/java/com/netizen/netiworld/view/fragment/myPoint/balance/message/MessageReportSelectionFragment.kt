package com.netizen.netiworld.view.fragment.myPoint.balance.message

import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.lifecycle.Observer
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentRevenueReportSelectionBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.MessageViewModel
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

class MessageReportSelectionFragment : Fragment() {

    private lateinit var binding: FragmentRevenueReportSelectionBinding
    private lateinit var messageViewModel: MessageViewModel

    private var calendar = Calendar.getInstance()
    private var dateSelection: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_revenue_report_selection,
            container,
            false
        )

        initView()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        messageViewModel = ViewModelProvider(activity!!,
            MessageViewModel.MessageBalanceViewModelFactory(context!!.applicationContext as Application))
            .get(MessageViewModel::class.java)
    }

    private fun initView() {
        val bundle = Bundle()
        bundle.putString("title", "Message Log")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.editTextFromDate.setOnClickListener {
            dateSelection = 1
            showDatePicker()
        }

        binding.editTextToDate.setOnClickListener {
            dateSelection = 2
            showDatePicker()
        }

        binding.buttonSearch.setOnClickListener {
            messageViewModel.checkMessageLogData(
                binding.editTextFromDate.text.toString(),
                binding.editTextToDate.text.toString()
            )
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
            }
        })

        messageViewModel.isFromDateEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(context!!, "From date is empty!", Toasty.LENGTH_LONG).show()
            }
        })

        messageViewModel.isToDateEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(context!!, "To date is empty!", Toasty.LENGTH_LONG).show()
            }
        })

        messageViewModel.isMessageLogDataFound.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_messageReportSelectionFragment_to_reportMessageRechargeFragment)
                messageViewModel.isMessageLogDataFound.value = false
            }
        })
    }

    private fun showDatePicker() {
        val datePickerDialog = DatePickerDialog(context!!, dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH))

        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        when(dateSelection) {
            1 -> {
                binding.editTextFromDate.setText(dateFormat.format(calendar.time))
            }
            2 -> {
                binding.editTextToDate.setText(dateFormat.format(calendar.time))
            }
        }
    }
}
