package com.netizen.netiworld.view.fragment.myPoint.purchase

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.ProductOfferReportAdapter
import com.netizen.netiworld.databinding.FragmentOfferProductReportBinding
import com.netizen.netiworld.model.ProductsOfferGetData
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.PurchaseViewModel

/**
 * A simple [Fragment] subclass.
 */
class OfferProductReportFragment : Fragment() {

    private lateinit var binding: FragmentOfferProductReportBinding
    private lateinit var purchaseViewModel: PurchaseViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_offer_product_report,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        purchaseViewModel = ViewModelProvider(
            activity!!,
            PurchaseViewModel.PurchaseViewModelFactory(context!!.applicationContext as Application)
        ).get(PurchaseViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Offer Product Report")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        purchaseViewModel.productOfferReportList.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                setAdapter(it)
            }
        })
    }

    private fun setAdapter(productOfferReportList: List<ProductsOfferGetData?>?) {
        binding.recyclerViewRevenueReport.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewRevenueReport.setHasFixedSize(true)
        binding.recyclerViewRevenueReport.adapter = ProductOfferReportAdapter(context!!, productOfferReportList)
    }
}
