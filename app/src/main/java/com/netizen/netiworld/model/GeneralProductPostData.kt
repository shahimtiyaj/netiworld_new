package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GeneralProductPostData {

    @SerializedName("purchaseQuantity")
    @Expose
    private var purchaseQuantity: Int? = null
    @SerializedName("unitPrice")
    @Expose
    private var unitPrice: Double? = null
    @SerializedName("totalAmount")
    @Expose
    private var totalAmount: Double? = null
    @SerializedName("vatAmount")
    @Expose
    private var vatAmount: Double? = null
    @SerializedName("paidAmount")
    @Expose
    private var paidAmount: Double? = null
    @SerializedName("productInfoDTO")
    @Expose
    private var productInfoDTO: ProductInfoDTO? = null
    @SerializedName("productRoleAssignDTO")
    @Expose
    private var productRoleAssignDTO: ProductRoleAssignDTO? = null

    fun getPurchaseQuantity(): Int? {
        return purchaseQuantity
    }

    fun setPurchaseQuantity(purchaseQuantity: Int?) {
        this.purchaseQuantity = purchaseQuantity
    }

    fun getUnitPrice(): Double? {
        return unitPrice
    }

    fun setUnitPrice(unitPrice: Double?) {
        this.unitPrice = unitPrice
    }

    fun getTotalAmount(): Double? {
        return totalAmount
    }

    fun setTotalAmount(totalAmount: Double?) {
        this.totalAmount = totalAmount
    }

    fun getVatAmount(): Double? {
        return vatAmount
    }

    fun setVatAmount(vatAmount: Double?) {
        this.vatAmount = vatAmount
    }

    fun getPaidAmount(): Double? {
        return paidAmount
    }

    fun setPaidAmount(paidAmount: Double?) {
        this.paidAmount = paidAmount
    }

    fun getProductInfoDTO(): ProductInfoDTO? {
        return productInfoDTO
    }

    fun setProductInfoDTO(productInfoDTO: ProductInfoDTO?) {
        this.productInfoDTO = productInfoDTO
    }

    fun getProductRoleAssignDTO(): ProductRoleAssignDTO? {
        return productRoleAssignDTO
    }

    fun setProductRoleAssignDTO(productRoleAssignDTO: ProductRoleAssignDTO?) {
        this.productRoleAssignDTO = productRoleAssignDTO
    }

    class ProductInfoDTO {

        @SerializedName("productID")
        @Expose
        private var productID: Int? = null

        fun getProductID(): Int? {
            return productID
        }

        fun setProductID(productID: Int?) {
            this.productID = productID
        }
    }

    class ProductRoleAssignDTO {

        @SerializedName("productRoleAssignID")
        @Expose
        private var productRoleAssignID: Int? = null

        fun getProductRoleAssignID(): Int? {
            return productRoleAssignID
        }

        fun setProductRoleAssignID(productRoleAssignID: Int?) {
            this.productRoleAssignID = productRoleAssignID
        }
    }
}

