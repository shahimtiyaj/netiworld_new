package com.netizen.netiworld.model

class ViewProfile {

    private var name: String? = null
    private var netiId: String? = null
    private var address: String? = null
    private var phone: String? = null
    private var email: String? = null
    private var gender: String? = null
    private var religion: String? = null
    private var birthday: String? = null

    constructor() {}

    constructor(
        name: String?,
        netiId: String?,
        address: String?,
        phone: String?,
        email: String?,
        gender: String?,
        religion: String?,
        birthday: String?
    ) {
        this.name = name
        this.netiId = netiId
        this.address = address
        this.phone = phone
        this.email = email
        this.gender = gender
        this.religion = religion
        this.birthday = birthday
    }


    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

}