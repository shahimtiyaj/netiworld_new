package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@JsonIgnoreProperties(ignoreUnknown = true)
class BalanceMessageGetData {

    @SerializedName("trxDate")
    @Expose
    private var trxDate: String? = null

    @SerializedName("quantity")
    @Expose
    private var quantity: Long? = null

    @SerializedName("productName")
    @Expose
    private var productName: String? = null

    @SerializedName("salesPrice")
    @Expose
    private var salesPrice: Double? = null

    private var payableAmount: Double? = null

    @SerializedName("productInfoDTO")
    @Expose
     var productInfoDTO: ProductInfoDTO? = null

    @SerializedName("productPurchaseLogDTO")
    @Expose
     var productPurchaseLogDTO: ProductPurchaseLogDTO? = null


    fun getTrxDate(): String?{
        return trxDate
    }

    fun setTrxDate(trxDate: String) {
        this.trxDate = trxDate
    }

    fun getQuantity(): Long?{
        return quantity
    }

    fun setQuantity(quantity: Long) {
        this.quantity = quantity
    }

    fun getSalesPrice(): Double?{
        return salesPrice
    }

    fun SetSalesPrice(salesPrice: Double) {
        this.salesPrice = salesPrice
    }

    class ProductInfoDTO {

        @SerializedName("productName")
        @Expose
        private var productName: String? = null

        fun getProductName(): String?{
            return productName
        }

        fun setProductName(productName: String) {
            this.productName = productName
        }
    }

    class ProductPurchaseLogDTO {

        @SerializedName("paidAmount")
        @Expose
        private var payableAmount: Double? = null

        fun getPayableAmount(): Double?{
            return payableAmount
        }

        fun setPayableAmount(payableAmount: Double) {
            this.payableAmount = payableAmount
        }
    }

}

