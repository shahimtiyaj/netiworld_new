package com.netizen.netiworld.apiService

import com.google.gson.JsonObject
import com.netizen.netiworld.model.*
import com.netizen.netiworld.model.ResetPassword
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("oauth/token")
    fun postData(@FieldMap params: HashMap<String?, String?>, @HeaderMap headers: HashMap<String?, String?>): Call<PostResponse>

    @GET("user/profile/details")
    fun getUserProfileInfo(@HeaderMap headers: HashMap<String?, String?>): Call<ProfileInformation>

    @GET("user/account/by/corebankid")
    fun getAccountNoInfo(@HeaderMap headers: HashMap<String?, String?>, @Query("coreBankID") localBankCategoryID: String): Call<MobileBankAcNo>

    @GET("user/category/by/type/2nd_parent_type")
    fun getBankAccountList(
        @Header("Authorization") token: String,
        @Query("typeDefaultCode") typeDefaultCode: String
    ): Call<List<BankAccountInfo>>

    @GET("user/message/types/by/point")
    fun getMessageType(@HeaderMap headers: HashMap<String?, String?>, @Query("roleID") code: String): Call<MessageType>

    //-------------------
    @GET("user/message/types/by/point")
    fun getMessageType1(@HeaderMap headers: HashMap<String?, String?>, @Query("roleID") code: String): Call<MessageType1>

    @GET("user/roles/assigned")
    fun getPurchasePoint(@HeaderMap headers: HashMap<String?, String?>): Call<PurchasePoint>

    @GET("user/products/by/role")
    fun getProductName(@HeaderMap headers: HashMap<String?, String?>, @Query("roleID") code: String?): Call<List<Products>>

    @POST("user/balance/deposit")
    fun submitDepositPostData(@HeaderMap headers: HashMap<String?, String?>, @Body obj: DepositPostData): Call<String>

    @POST("user/message/recharge")
    fun messageRechargepostData(@Body obj: MessageRecharge, @HeaderMap headers: HashMap<String?, String?>): Call<Void>

    @POST("user/product/purchase")
    fun generalProductPostData(@Body obj: GeneralProductPostData, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @GET("user/product/offer/by/code")
    fun getOfferProduct(@HeaderMap headers: HashMap<String?, String?>, @Query("code") code: String): Call<OfferProduct>

    @POST("user/product/purchase/offer")
    fun offerProductPostData(@Body obj: OfferProductPost, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @GET("user/profile/by/custom_id")
    fun getDataSearchByNetiID(@HeaderMap headers: HashMap<String?, String?>, @Query("custom_id") code: String): Call<String>

    @POST("user/transfer/check/balance")
    fun WalletBalanceTransfer(@Body obj: WalletTransfer, @HeaderMap headers: HashMap<String?, String?>): Call<Void>

    @GET("guest/core/check-otp")
    fun OTPVarify(@HeaderMap headers: HashMap<String?, String?>, @Query("code") code: String): Call<String>

    @POST("user/balance/transfer")
    fun WalletBalanceTransferFinal(@Body obj: WalletTransfer, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @POST("user/balance/requests/by/date_range")
    fun balanceDepositReportGetData(
        @HeaderMap headers: HashMap<String?, String?>,
        @Body obj: BalanceReportPostData
    ): Call<List<BalanceDepositReportGetData>>

    /*@POST("user/get-transfer-records")
    fun balanceTransferGetData(@Body obj: BalanceDepositPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceTransferGetData>>
    */
    @POST("user/balance/transfer/by/date")
    fun balanceTransferGetData(
        @HeaderMap headers: HashMap<String?, String?>,
        @Body obj: BalanceReportPostData
    ): Call<List<BalanceTransferGetData>>

    @POST("user/message/by/date-range")
    fun messageRechargeGetData(@Body obj: BalanceDepositPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceMessageGetData>>

    @POST("user/balance/statement/for/android")
    fun balanceStatementGetData(@HeaderMap headers: HashMap<String?, String?>,
                                @Body obj: BalanceDepositPostForList
    ): Call<List<BalanceStatementGetData>>

    @GET("user/product/purchases/by/date-range")
    fun getGeneralProductReportData(
        @HeaderMap headers: HashMap<String?, String?>,
        @Query("startDate") startDate: String,
        @Query("endDate") endDate: String
    ): Call<List<GeneralProductGetData>>

    @GET("user/purchase/codes/by/purchaseid")
    fun getProductGDetailsData(@HeaderMap headers: HashMap<String?, String?>, @Query("purchaseID") purchaseID: Int?): Call<List<ProductGDetailsGetData>>

    @POST("user/product/offer/by/date-range")
    fun getProductOfferReportData(
        @HeaderMap headers: HashMap<String?, String?>,
        @Body obj: ProductsOfferReportPostData
    ): Call<List<ProductsOfferGetData>>

    @GET("user/purchase/codes/by/purchaseid")
    fun getProductODetailsData(@HeaderMap headers: HashMap<String?, String?>, @Query("purchaseID") purchaseID: Int?): Call<List<ProductGDetailsGetData>>

    @GET("user/purchase/codes/by/usedstatus")
    fun getPurchaseCodeData(@HeaderMap headers: HashMap<String?, String?>, @Query("usedStatus") usedStatus: Int?): Call<List<PurchaseCodeLogGetdata>>

    @GET("guest/file/find")
    fun getProfile(@HeaderMap headers: HashMap<String?, String?>, @Query("filePath") filePath: String): Call<ProfileImage>

    @POST("guest/user/register-user")
    fun registrationSubmit(@Body obj: RegistrationPost): Call<Void>

    @GET("guest/addresses/district")
    fun getDistrict(): Call<List<DistrictModel>>

    @GET("guest/addresses/area")
    fun getArea(@Query("parentCategoryID") parentCategoryID: String): Call<List<AreaModel>>

    @GET("guest/user/profile/by")
    fun userCheckForgotPaasword(@Query("username") username: String): Call<UserCheckForgotPass>

      @POST("guest/recover/password/check_contact")
    fun forgotPasswordOTP(@Query("userContactNo") userContactNo: String, @Body obj: UserCheckForgotPass?): Call<Void>

    @GET("guest/core/check-otp")
    fun forgotPasswordOTPVarify(@Query("code") code: String): Call<Any>

    @POST("guest/recover/password/reset")
    fun resetPasswordAfterForgot(@Body obj: ResetPassword?): Call<Void>

    @POST("user/change/password")
    fun changePassword(@HeaderMap headers: HashMap<String?, String?>, @Body obj: ResetPassword?): Call<Void>

    //------------------------------------------------------------------------------------------------------
    @Headers(
        "Authorization: Basic ZGV2Z2xhbi1jbGllbnQ6ZGV2Z2xhbi1zZWNyZXQ=",
        "Content-Type:application/x-www-form-urlencoded",
        "NZUSER:absiddik:123:password"
    )
    @FormUrlEncoded
    @POST("oauth/token")
    fun userLogIn(@Field("name") name: String, @Field("password") password: String, @Field("grant_type") grant_type: String): Call<MSG>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/home/dashboard/info")
    fun getHomePageInfo(@Header("Authorization") token: String): Call<HomePageInfo>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/core/bank/accounts/by")
    fun getBalanceDepositAccountInfo(@Header("Authorization") token: String,
                                     @Query("defaultCode") defaultCode: String
    ): Call<List<BalanceDepositAccountInfo>?>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tagged/bank-account/by")
    fun getBalanceWithdrawInfo(@Header("Authorization") token: String,
                               @Query("coreCategoryDefaultCode") codeCategoryDefaultCode: String
    ): Call<WithDrawInfo?>

    @POST("user/balance/withdraw")
    fun submitBalanceWithdrawData(@Header("Authorization") token: String,
                                  @Body obj: WithdrawPostData
    ): Call<String?>

    @POST("user/report/revenue/log")
    fun getRevenueLogData(@Header("Authorization") token: String,
                          @Body revenueLogPostData: RevenueLogPostData?
    ): Call<RevenueLogGetData>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/category/by/typeDefaultCode")
    fun getBankDistrictList(@Header("Authorization") token: String,
                            @Query("typeDefaultCode") typeDefaultCode: String?
    ): Call<List<BankAccountInfo>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/bank/branches/by")
    fun getBankBranchList(@Header("Authorization") token: String,
                          @Query("bankID") bankId: String?,
                          @Query("districtID") districtId: String?
    ): Call<List<BankBranchInfo>>

    @POST("user/bank/add-account")
    fun submitBankAccount(@Header("Authorization") token: String,
                          @Body bankAccountPostData: BankAccountPostData
    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/bank/accounts/by/user")
    fun getBankList(@Header("Authorization") token: String): Call<List<BankList>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/category/by/typeDefaultCode")
    fun getTagTypeList(@Header("Authorization") token: String,
                       @Query("typeDefaultCode") typeDefaultCode: String
    ): Call<List<TagType>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/bank/account/taggings")
    fun getTagList(@Header("Authorization") token: String): Call<List<TagGetData>>

    @POST("user/bank/account/tag")
    fun tagBankAccount(@Header("Authorization") token: String,
                       @Body tagPostData: TagPostData
    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tokens/modules/by")
    fun getProblemModuleList(@Header("Authorization") token: String,
                             @Query("catDefaultCode") catDefaultCode: String
    ): Call<List<Problem>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tokens/types/by")
    fun getProblemTypeList(@Header("Authorization") token: String,
                           @Query("parentCategoryID") parentCategoryID: String
    ): Call<List<Problem>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tokens/by/pending")
    fun getSolvedAndPendingToken(@Header("Authorization") token: String): Call<List<TokenList>>

    @POST("user/tokens/new")
    fun submitNewToken(@Header("Authorization") token: String,
                       @Body tokenSubmitPostData: TokenSubmitPostData
    ): Call<Void>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tokens/by/top10")
    fun getTokenList(@Header("Authorization") token: String): Call<List<TokenList>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("guest/file/find/")
    fun downloadTokenAttachment(@Header("Authorization") token: String,
                                @Query("filePath") imageUrl: String?
    ): Call<JsonObject>
}