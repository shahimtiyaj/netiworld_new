package com.netizen.netiworld.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netizen.netiworld.apiService.ApiClient
import com.netizen.netiworld.apiService.ApiInterface
import com.netizen.netiworld.model.*
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import com.orhanobut.logger.Logger
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class WalletRepository(application: Application) {

    private val appPreferences = AppPreferences(application)

    var accountInfoList = MutableLiveData<List<BalanceDepositAccountInfo>>()
    var withDrawInfo = MutableLiveData<WithDrawInfo>()
    var fullName = MutableLiveData<String>()
    var phoneNo = MutableLiveData<String>()
    var netiMainID = MutableLiveData<String>()
    var customNetiId = MutableLiveData<String>()
    var isDepositDataFound = MutableLiveData<Boolean>()
    var isWithdrawDataFound = MutableLiveData<Boolean>()
    var isTransferDataFound = MutableLiveData<Boolean>()

    val balanceTransferArrayList = MutableLiveData<List<BalanceTransferGetData>>()
    val balanceStatementArrayList = MutableLiveData<List<BalanceStatementGetData>>()
    var isBalanceSatementDataFound = MutableLiveData<Boolean>()

    val balanceDepositReportDataList = MutableLiveData<List<BalanceDepositReportGetData>>()
    val balanceTransferReportDataList = MutableLiveData<List<BalanceTransferGetData>>()

    fun getAccountInfo() {
        Loaders.isLoading1.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getBalanceDepositAccountInfo(
            "bearer " + appPreferences.getToken().toString(),
            "T10103"
        )

        call.enqueue(object : Callback<List<BalanceDepositAccountInfo>?> {
            override fun onFailure(call: Call<List<BalanceDepositAccountInfo>?>, t: Throwable) {
                Loaders.isLoading1.value = false
                Loaders.error?.value = "Couldn't get bank account information! Please try again."
            }

            override fun onResponse(
                call: Call<List<BalanceDepositAccountInfo>?>,
                response: Response<List<BalanceDepositAccountInfo>?>
            ) {
                try {
                    if (response.isSuccessful) {
                        accountInfoList.value = response.body() as List<BalanceDepositAccountInfo>
                    } else {
                        Loaders.error?.value = response.message().toString()
                    }

                    Loaders.isLoading1.value = false
                } catch (e: Exception) {
                    Loaders.isLoading1.value = false
                    Loaders.error?.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun submitDepositPostData(depositPostData: DepositPostData) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        header["Content-Type"] = "application/json"

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.submitDepositPostData(header, depositPostData)

        call.enqueue(object : Callback<String?> {
            override fun onFailure(call: Call<String?>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.error?.value = "Couldn't submit deposit information! Please try again."
            }

            override fun onResponse(
                call: Call<String?>,
                response: Response<String?>
            ) {
                try {
                    if (response.isSuccessful) {
                        Loaders.success?.value = "Deposit data submitted successfully."
                    } else {
                        Loaders.error?.value =
                            "Couldn't submit deposit information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.error?.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getWithdrawInfo() {
        Loaders.isLoading1.value = true
        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getBalanceWithdrawInfo(
            "bearer " + appPreferences.getToken().toString(),
            "C123001"
        )

        call.enqueue(object : Callback<WithDrawInfo?> {
            override fun onFailure(call: Call<WithDrawInfo?>, t: Throwable) {
                Loaders.isLoading1.value = false
                Loaders.apiError?.value = "Couldn't get withdraw information! Please try again."
            }

            override fun onResponse(
                call: Call<WithDrawInfo?>,
                response: Response<WithDrawInfo?>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<WithDrawInfo>() {}.type
                        val errorResponse: WithDrawInfo? =
                            Gson().fromJson(response.errorBody()!!.charStream(), type)
                        withDrawInfo.value = errorResponse
                    } else {
                        Loaders.apiError?.value = response.message().toString()
                    }

                    Loaders.isLoading1.value = false
                } catch (e: Exception) {
                    Loaders.isLoading1.value = false
                    Loaders.apiError?.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun submitWithdrawData(withdrawPostData: WithdrawPostData) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.submitBalanceWithdrawData(
            "bearer " + appPreferences.getToken().toString(),
            withdrawPostData
        )

        call.enqueue(object : Callback<String?> {
            override fun onFailure(call: Call<String?>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError?.value = "Couldn't submit withdraw information! Please try again."
            }

            override fun onResponse(
                call: Call<String?>,
                response: Response<String?>
            ) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess?.value = "Withdraw data submitted successfully."
                    } else {
                        Loaders.apiError?.value =
                            "Couldn't submit withdraw information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError?.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getPersonInfoForWalletTransfer(customNetiID: String) {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        Log.d("Custom Neti Id: ", customNetiID)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getDataSearchByNetiID(header, customNetiID)

        userCall?.enqueue(object : Callback<String> {

            override fun onResponse(call: Call<String>, response: Response<String>) {

                try {
                    Log.d(
                        "onResponse",
                        "Main Wallet Transfer response :" + response.body().toString()
                    )

                    if (response.code() == 302) {
                        val walletTransfer = response.errorBody()?.source()?.buffer()?.readUtf8()
                        val jsonObj = JSONObject(walletTransfer!!)
                        fullName.postValue(jsonObj.getString("fullName"))
                        phoneNo.postValue(jsonObj.getString("basicMobile"))
                        netiMainID.postValue(jsonObj.getString("netiID"))
                        customNetiId.postValue(jsonObj.getString("customNetiID"))

                    } else {
                        Loaders.error.value = "Opps ! Something Wrong"
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    Loaders.error.value = "Something went wrong! Please try again."
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.error.value = "Couldn't transfer wallet balance! Please try again."
            }
        })
    }

    fun requestForOTP(TransferAmount: String, mainNetiID: String?, note: String) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        header["Content-Type"] = "application/json"
        try {
            val balanceWalletTransfer = WalletTransfer(
                TransferAmount,
                WalletTransfer.Receiver(mainNetiID?.toLongOrNull()),
                note
            )

            Log.d("Transfer wallet:", "JSON:" + balanceWalletTransfer)

            val service = ApiClient.getClient?.create(ApiInterface::class.java)
            val call = service?.WalletBalanceTransfer(balanceWalletTransfer, header)

            //calling the api------------------------------------------------------------
            call?.enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    try {
                        Log.d("onResponse", "Balance Wallet post:" + response.body())
                        Logger.d("Balance Wallet post:", response.body().toString())

                        when {
                            response.code() == 200 -> {
                                Log.d("onResponse", "Success:" + response.body())
                                Loaders.apiSuccessforOTP.value = "6 Digit OTP code has sent"
                            }
                            response.code() == 400 -> {
                                Loaders.error.value = "Insufficient Wallet Balance"
                            }
                            response.code() == 409 -> {
                                Loaders.error.value = "Duplicate Entry"
                            }
                            response.code() == 500 -> {
                                Loaders.error.value = "An Error Occurred"
                            }
                            else -> {
                                Loaders.error.value = "Opps!! Something Wrong"
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Loaders.apiError.value = "Something went wrong! Please try again."
                        Loaders.isLoading0.value = false
                    }
                    Loaders.isLoading0.value = false
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Balance Wallet Check Fail !"
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
            Loaders.isLoading0.value = false

        } catch (e: NullPointerException) {
            e.printStackTrace()
            Loaders.isLoading0.value = false
        }
    }

    fun varifyOTP(OTP: String) {

        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        header["Content-Type"] = "application/json"
        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.OTPVarify(header, OTP)

        userCall?.enqueue(object : Callback<String> {

            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("onResponse", "OTP Check response :" + response.body().toString())

                if (response.code() == 200) {
                    Loaders.apiSuccessforOTPVarify.value = "OTP Verified"
                } else {
                    Loaders.apiErrorOTPInvalid.value = "X Incorrect OTP"
                }
                Loaders.isLoading0.value = false
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
            }
        })
    }

    fun transferWalletBalance(TransferAmount: String, netiMainID: String, note: String) {

        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        header["Content-Type"] = "application/json"
        try {

            val balanceWalletTransfer = WalletTransfer(
                TransferAmount,
                WalletTransfer.Receiver(netiMainID.toLongOrNull()),
                note
            )

            Log.d("Transfer wallet final:", "JSON:" + balanceWalletTransfer)

            val service = ApiClient.getClient?.create(ApiInterface::class.java)
            val call = service?.WalletBalanceTransferFinal(balanceWalletTransfer, header)

            //calling the api------------------------------------------------------------
            call?.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    Log.d("onResponse", "Balance Wallet post final:" + response.body())
                    Logger.d("Balance Wallet post final:", response.body().toString())

                    if (response.code() == 202) {
                        Log.d("onResponse", "Success final:" + response.body())
                        Loaders.apiSuccessBalanceTransfer.value =
                            "Balance Wallet Transferred Successfully"
                    } else if (response.code() == 400) {
                        Loaders.error.value = "Insufficient Wallet Balance"
                    }

                    Loaders.isLoading0.value = false
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Balance Wallet Transfer Fail !!"
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
            Loaders.isLoading0.value = false
        } catch (e: NullPointerException) {
            e.printStackTrace()
            Loaders.isLoading0.value = false
        }
    }

    //Wallet Balance Statement reports---------------------------------------------------------------

    fun getBalanceStatementListData(startDateStatement: String, endDateStatement: String) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        header["Content-Type"] = "application/json"

        val balanceDeposit = BalanceDepositPostForList(
            startDateStatement,
            endDateStatement,
            0,
            50
        )

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.balanceStatementGetData(header, balanceDeposit)
        val emptyList = ArrayList<BalanceTransferGetData>()

        //calling the api
        call?.enqueue(object : Callback<List<BalanceStatementGetData>> {
            override fun onResponse(
                call: Call<List<BalanceStatementGetData>>,
                response: Response<List<BalanceStatementGetData>>
            ) {
                try {
//                    Log.d("onResponse", "Balance Deposit:" + response.body())

                    if (response.code() == 200) {
                        Log.d("onResponse", "Token:" + response.body())
                        balanceStatementArrayList.postValue(response.body() as ArrayList<BalanceStatementGetData>?)
                        isBalanceSatementDataFound.value = true
                    } else {
                        isBalanceSatementDataFound.value = false
                        balanceTransferArrayList.postValue(emptyList)
                        Loaders.apiError.value = "Sorry ! No Data Found."
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    isBalanceSatementDataFound.value = false
                    balanceTransferArrayList.postValue(emptyList)
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }

                Loaders.isLoading0.value = false
            }

            override fun onFailure(call: Call<List<BalanceStatementGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                balanceTransferArrayList.postValue(emptyList)
                isBalanceSatementDataFound.value = false
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Balance Statement Get Data Fail"
            }
        })
    }

    fun getBalanceReportData(balanceReportPostData: BalanceReportPostData, type: String) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.balanceDepositReportGetData(header, balanceReportPostData)

        call.enqueue(object : Callback<List<BalanceDepositReportGetData>> {
            override fun onFailure(call: Call<List<BalanceDepositReportGetData>>, t: Throwable) {
                Loaders.isLoading0.value = false
                isDepositDataFound.value = false
                isWithdrawDataFound.value = false
                Loaders.apiError.value = "Couldn't get report data! Please try again."
            }

            override fun onResponse(
                call: Call<List<BalanceDepositReportGetData>>,
                response: Response<List<BalanceDepositReportGetData>>
            ) {
                try {
                    if (response.isSuccessful) {
                        balanceDepositReportDataList.value = response.body()

                        if (balanceDepositReportDataList.value!!.isNotEmpty()) {
                            if (type.equals("Deposit", true)) {
                                isDepositDataFound.value = true
                            } else {
                                isWithdrawDataFound.value = true
                            }
                        } else {
                            isDepositDataFound.value = false
                            isWithdrawDataFound.value = false
                            Loaders.apiError.value = "No data found!"
                        }
                    } else {
                        isDepositDataFound.value = false
                        isWithdrawDataFound.value = false
                        Loaders.apiError.value = response.message().toString()
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isDepositDataFound.value = false
                    isWithdrawDataFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getBalanceTransferReportData(balanceReportPostData: BalanceReportPostData) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.balanceTransferGetData(header, balanceReportPostData)

        call.enqueue(object : Callback<List<BalanceTransferGetData>> {
            override fun onFailure(call: Call<List<BalanceTransferGetData>>, t: Throwable) {
                Loaders.isLoading0.value = false
                isTransferDataFound.value = false
                Loaders.apiError.value =
                    "Couldn't get balance transfer report data! Please try again."
            }

            override fun onResponse(
                call: Call<List<BalanceTransferGetData>>,
                response: Response<List<BalanceTransferGetData>>
            ) {
                try {
                    if (response.isSuccessful) {
                        balanceTransferReportDataList.value = response.body()

                        if (balanceTransferReportDataList.value!!.isNotEmpty()) {
                            isTransferDataFound.value = true
                        } else {
                            isTransferDataFound.value = false
                            Loaders.apiError.value = "No data found!"
                        }
                    } else {
                        isTransferDataFound.value = false
                        Loaders.apiError.value = response.message().toString()
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isTransferDataFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }
}