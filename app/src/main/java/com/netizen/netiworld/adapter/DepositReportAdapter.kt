package com.netizen.netiworld.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleRevenueReportLayoutBinding
import com.netizen.netiworld.model.BalanceDepositReportGetData
import com.netizen.netiworld.utils.AppUtilsClass

class DepositReportAdapter(private val context: Context,
                           private val balanceDepositReportList: List<BalanceDepositReportGetData?>?
) : RecyclerView.Adapter<DepositReportAdapter.ViewHolder>() {

    class ViewHolder(private val itemBinding: SingleRevenueReportLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(balanceDepositReportGetData: BalanceDepositReportGetData?) {
            itemBinding.textViewDate.text = AppUtilsClass.getDate(balanceDepositReportGetData?.getRequestDate()?.toLong())
            itemBinding.textViewType.text = balanceDepositReportGetData?.getRequestType()
            itemBinding.textViewAmount.text = balanceDepositReportGetData?.getRequestedAmount().toString() + " Taka"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.single_revenue_report_layout,
            parent,
            false
        ))
    }

    override fun getItemCount(): Int {
        return balanceDepositReportList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(balanceDepositReportList?.get(position))
    }
}