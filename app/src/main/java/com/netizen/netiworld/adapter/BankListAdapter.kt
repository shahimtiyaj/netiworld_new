package com.netizen.netiworld.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleBankListLayoutBinding
import com.netizen.netiworld.model.BankList
import com.netizen.netiworld.model.TagGetData

class BankListAdapter(private val context: Context,
                      private val bankList: List<BankList>,
                      private val tagList: List<TagGetData>,
                      private val listener: OnTagClickListener
) : RecyclerView.Adapter<BankListAdapter.ViewHolder>() {

    interface OnTagClickListener {
        fun onTagClick(userBankAccountId: String, netiId: String) {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_bank_list_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return bankList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(bankList[position], tagList)

        holder.itemBinding.textViewAddTag.setOnClickListener {
            listener.onTagClick(bankList[position].getUserBankAccountID().toString(), bankList[position].getNetiID().toString())
        }
    }

    class ViewHolder(val itemBinding: SingleBankListLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(bankList: BankList, tagList: List<TagGetData>) {
            itemBinding.textViewBankName.text = bankList.getBank()
            itemBinding.textViewBranch.text = bankList.getBranchName()
            itemBinding.textViewRoutingNumber.text = bankList.getRoutingNumber()
            itemBinding.textViewAccountName.text = bankList.getHolderName().toString()
            itemBinding.textViewAccountNumber.text = bankList.getAccNumber().toString()

            tagList.forEach { tagGetData ->
                if (bankList.getUserBankAccountID() == tagGetData.getUserBankAccountInfoDTO()!!.getUserBankAccId()) {
                    itemBinding.textViewTagFor.visibility = View.VISIBLE
                    itemBinding.layoutAddTag.visibility = View.GONE
                    itemBinding.textViewTagFor.text = tagGetData.getTaggingTypeCoreCategoryInfoDTO()!!.getCategoryName().toString()
                    return
                }
            }
        }
    }
}