package com.netizen.netiworld.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.netizen.netiworld.view.fragment.myPoint.balance.wallet.DepositFragment
import com.netizen.netiworld.view.fragment.myPoint.balance.wallet.TransferFragment
import com.netizen.netiworld.view.fragment.myPoint.balance.wallet.WithdrawFragment

class WalletPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return DepositFragment()
            1 -> return WithdrawFragment()
            2 -> return TransferFragment()
        }
        return DepositFragment()
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "Deposit"
            1 -> return "Withdraw"
            2 -> return "Transfer"
        }
        return null
    }
}