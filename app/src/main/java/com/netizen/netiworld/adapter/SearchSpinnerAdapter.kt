package com.netizen.netiworld.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleSpinnerLayoutBinding
import com.netizen.netiworld.utils.Loaders

class SearchSpinnerAdapter(private val context: Context,
                           private val sectionList: List<String>,
                           private val spinnerType: Int?
) : RecyclerView.Adapter<SearchSpinnerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_spinner_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return sectionList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemBinding.textViewSpinnerItem.text = sectionList[position]
        if (position == (sectionList.size - 1)) {
            holder.itemBinding.viewUnderline.visibility = View.GONE
        }

        holder.itemBinding.textViewSpinnerItem.setOnClickListener {
            when(spinnerType) {
                1 -> {
                    Loaders.searchValue1?.postValue(sectionList[position])
                }
                2 -> {
                    Loaders.searchValue2?.postValue(sectionList[position])
                }
                3 -> {
                    Loaders.searchValue3?.postValue(sectionList[position])
                }
            }
        }
    }

    class ViewHolder(val itemBinding: SingleSpinnerLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root)
}