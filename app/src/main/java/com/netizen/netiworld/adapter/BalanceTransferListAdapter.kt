package com.netizen.netiworld.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.ReportsWalletBalanceTransferRowBinding
import com.netizen.netiworld.model.BalanceTransferGetData
import com.netizen.netiworld.utils.AppUtilsClass

class BalanceTransferListAdapter(
    private val context: Context,
    private val balanceTransferReportsList: List<BalanceTransferGetData?>
) : RecyclerView.Adapter<BalanceTransferListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.reports_wallet_balance_transfer_row,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return balanceTransferReportsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(balanceTransferReportsList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: ReportsWalletBalanceTransferRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(balanceReport: BalanceTransferGetData?) {
            itemBinding.requestDate.text = balanceReport?.transactionDate?.toLong()?.let { AppUtilsClass.getDate(it) }
            if (balanceReport?.transactionFor=="Receive"){
                itemBinding.transTypeVal.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_left_black_24dp, 0);
            }
            else if(balanceReport?.transactionFor=="Send"){
                itemBinding.transTypeVal.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_right_black_24dp, 0);
            }

            itemBinding.transTypeVal.text = balanceReport?.transactionFor
            itemBinding.netiIdVal.text = balanceReport?.netiID.toString()
            itemBinding.nameVal.text = balanceReport?.fullName
            itemBinding.mobileNoVal.text = balanceReport?.basicMobile
            itemBinding.noteVal.text = balanceReport?.note
            itemBinding.transAmountVal.text = String.format("%, .2f", balanceReport?.amount)
        }
    }

    companion object {
        private val TAG = BalanceTransferListAdapter::class.java.simpleName
    }
}