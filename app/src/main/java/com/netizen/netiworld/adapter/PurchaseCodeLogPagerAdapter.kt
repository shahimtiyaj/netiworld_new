package com.netizen.netiworld.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.netizen.netiworld.view.fragment.myPoint.purchase.UnusedPurchaseReportFragment
import com.netizen.netiworld.view.fragment.myPoint.purchase.UsedPurchaseReportFragment
import com.netizen.netiworld.view.fragment.myPoint.balance.wallet.DepositFragment

class PurchaseCodeLogPagerAdapter (manager: FragmentManager) : FragmentPagerAdapter(manager) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return UnusedPurchaseReportFragment()
            1 -> return UsedPurchaseReportFragment()
        }
        return DepositFragment()
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "Unused Purchase Code"
            1 -> return "Used Purchase Code"
        }
        return null
    }
}