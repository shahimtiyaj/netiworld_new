package com.netizen.netiworld.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.netizen.netiworld.view.fragment.myPoint.balance.wallet.DepositFragment
import com.netizen.netiworld.view.fragment.myPoint.balance.wallet.DepositReportSelectionFragment
import com.netizen.netiworld.view.fragment.myPoint.balance.wallet.TransferReportSelectionFragment
import com.netizen.netiworld.view.fragment.myPoint.balance.wallet.WithdrawReportSelectionFragment

class WalletReportPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return DepositReportSelectionFragment()
            1 -> return WithdrawReportSelectionFragment()
            2 -> return TransferReportSelectionFragment()
        }
        return DepositFragment()
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "Deposit"
            1 -> return "Withdraw"
            2 -> return "Transfer"
        }
        return null
    }
}