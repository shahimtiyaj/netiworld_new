package com.netizen.netiworld.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleRevenueReportLayoutBinding
import com.netizen.netiworld.model.ProductsOfferGetData
import com.netizen.netiworld.utils.AppUtilsClass

class ProductOfferReportAdapter(private val context: Context,
                                private val productOfferReportList: List<ProductsOfferGetData?>?
) : RecyclerView.Adapter<ProductOfferReportAdapter.ViewHolder>() {

    class ViewHolder(private val itemBinding: SingleRevenueReportLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(productsOfferGetData: ProductsOfferGetData?) {
            itemBinding.textViewDate.text = AppUtilsClass.getDate(productsOfferGetData?.getOfferUseDate()?.toLong())
            itemBinding.textViewType.text = productsOfferGetData?.getProductType()
            itemBinding.textViewAmount.text = String.format("%.2f", productsOfferGetData?.getPayableAmount()) + " Taka"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.single_revenue_report_layout,
            parent,
            false
        ))
    }

    override fun getItemCount(): Int {
        return productOfferReportList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(productOfferReportList?.get(position))
    }
}