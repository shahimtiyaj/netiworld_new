package com.netizen.netiworld.utils

import android.Manifest
import android.app.Activity
import android.app.Application
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.provider.MediaStore
import android.text.TextUtils
import android.text.format.DateFormat
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.ContextCompat
import es.dmoral.toasty.Toasty
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class AppUtilsClass {

    companion object {

        private const val IMAGE_PICK_CODE = 1000
        private const val PERMISSION_CODE_READ = 1001
        private const val PERMISSION_CODE_WRITE = 1002
        private const val PERMISSION_CODE_CAMERA = 1003

        fun getDateFormat(date: String): String {
            val dateFormatprev = SimpleDateFormat("yyyy-mm-dd")
            val dateFormatprevParse = dateFormatprev.parse(date)
            val dateFormat = SimpleDateFormat("dd/mm/yyyy")
            val userDateOfBirth = dateFormat.format(dateFormatprevParse!!)

            return userDateOfBirth
        }

        fun getDateFormatForSerachData(date: String): String {
            val dateFormatprev = SimpleDateFormat("dd/mm/yyyy") //dd/mm/yyyy
            val dateFormatprevParse = dateFormatprev.parse(date)
            val dateFormat = SimpleDateFormat("yyyy-mm-dd")
            val userDateOfBirth = dateFormat.format(dateFormatprevParse!!)

            return userDateOfBirth
        }

        fun getDate(time: Long?): String? {
            val cal = Calendar.getInstance(Locale.ENGLISH)

            if (time != null) {
                cal.timeInMillis = time
            }

            return DateFormat.format("dd MMM, yyyy", cal).toString()
        }

        fun showErrorToasty(context: Context, message: String) {
            Toasty.error(context, message, Toasty.LENGTH_SHORT).show()
        }

        fun showsDatePicker(context: Context, dateListener: DatePickerDialog.OnDateSetListener) {
            val calendar= Calendar.getInstance()
            val datePickerDialog = DatePickerDialog(context, dateListener,
                Calendar.getInstance().get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
        }

        fun isValidPassword(password: String?): Boolean {
            val pattern: Pattern
            val matcher: Matcher
            val PASSWORD_PATTERN =
                "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"
            pattern = Pattern.compile(PASSWORD_PATTERN)
            matcher = pattern.matcher(password)
            return matcher.matches()
        }

         fun isValidEmail(email: String): Boolean {
            return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email)
                .matches()
        }

        fun validCellPhone(number: String?): Boolean {
            return Patterns.PHONE.matcher(number).matches()
        }

        fun requestFocus(view: View, context: Activity) {
            if (view.requestFocus()) {
                context.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
        }

        fun getDecimalFormattedValue(value: Double): String {
            val decimalFormat = DecimalFormat("###,###,###,###.##")
            return decimalFormat.format(value)
        }

        fun checkPermissionForImage(activity: Activity): Boolean {
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
                ||
                ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE),
                    PERMISSION_CODE_CAMERA
                )

                return false
            } else {
                return true
            }
        }
    }
}