package com.netizen.netiworld.utils

import android.content.Context
import android.content.SharedPreferences

class AppPreferences(context: Context?) {

    private val PREFERENCE_NAME = "app_preferences"
    private val KEY_TOKEN = "access_token"
    private val KEY_IS_SPLASH_SHOWN = "is_splash_shown"
    private var mContext: Context? = null
    private var mPreferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null

    private val KEY_PURCHASE_POINT = "purchase_point"
    private val KEY_MESSAGE_TYPE = "message_type"

    init {
        mContext = context
        mPreferences = mContext!!.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        editor = mPreferences!!.edit()
    }

    fun setToken(token: String?) {
        editor!!.putString(KEY_TOKEN, token)
        editor!!.apply()
    }

    fun getToken(): String? {
        var token: String? = null
        if (mPreferences!!.contains(KEY_TOKEN)) {
            token = mPreferences!!.getString(KEY_TOKEN, "")
        }
        return token
    }

    fun setIsSplashShown() {
        editor!!.putBoolean(KEY_IS_SPLASH_SHOWN, true)
        editor!!.apply()
    }

    fun isSplashShown(): Boolean {
        return mPreferences!!.getBoolean(KEY_IS_SPLASH_SHOWN, false)
    }

    fun getPurchasePoint(): String? {
        return mPreferences!!.getString(KEY_PURCHASE_POINT, "0")
    }

    fun setPurchasePoint(purchasePointName: String) {
        editor!!.putString(KEY_PURCHASE_POINT, purchasePointName)
        editor!!.apply()
    }

    fun getMessageType(): String? {
        return mPreferences!!.getString(KEY_MESSAGE_TYPE, "0")
    }

    fun setMessageType(messageName: String) {
        editor!!.putString(KEY_MESSAGE_TYPE, messageName)
        editor!!.apply()
    }
}