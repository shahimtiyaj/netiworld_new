package com.netizen.netiworld.utils

import androidx.lifecycle.MutableLiveData

class Loaders {

    companion object {
        val isLoading0 = MutableLiveData<Boolean>()
        val isLoading1 = MutableLiveData<Boolean>()
        val isLoading2 = MutableLiveData<Boolean>()
        val isLoading3 = MutableLiveData<Boolean>()
        val isLoading4 = MutableLiveData<Boolean>()
        val isToolbarSettingClicked = MutableLiveData<Boolean>()
        val isToolbarNotificationClicked = MutableLiveData<Boolean>()
        val isLogOut = MutableLiveData<Boolean>()

        val error = MutableLiveData<String?>()
        val success = MutableLiveData<String?>()
        val searchValue1 = MutableLiveData<String?>()
        val searchValue2 = MutableLiveData<String?>()
        val searchValue3 = MutableLiveData<String?>()
        val apiError = MutableLiveData<String?>()
        val apiSuccess = MutableLiveData<String?>()

        val apiSuccessforOTP = MutableLiveData<String>()
        val apiSuccessforOTPVarify = MutableLiveData<String>()
        val apiErrorOTPInvalid = MutableLiveData<String>()
        val apiSuccessBalanceTransfer = MutableLiveData<String>()
    }
}