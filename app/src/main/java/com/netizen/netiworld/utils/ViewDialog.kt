package com.netizen.netiworld.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.Window
import com.netizen.netiworld.R

class ViewDialog(internal var activity: Activity) {
    internal lateinit var dialog: Dialog

    fun showDialog() {

        dialog = Dialog(activity as Context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.login_invalid)

        dialog.show()
    }

    fun hideDialog() {
        dialog.dismiss()
    }
}